package controller;


import java.net.URI;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import view.GuiHandler;

/**
 * This example demonstrates how to create a websocket connection to a server.
 * Only the most important callbacks are overloaded.
 */
public class WebSocketCl extends WebSocketClient
{
	GuiHandler view;

	public WebSocketCl(URI serverURI, GuiHandler view)
	{
		super(serverURI);
		this.view = view;
	}

	@Override
	public void onOpen(ServerHandshake handshakedata)
	{
		send("Hello, it is me. :)");
		view.getTextArea().appendText("Opened connection\n");
		
		// if you plan to refuse connection based on ip or httpfields overload:
		// onWebsocketHandshakeReceivedAsClient
	}

	@Override
	public void onMessage(String message)
	{
		view.getTextArea().appendText("reveived: " + message + "\n");
	}

	@Override
	public void onClose(int code, String reason, boolean remote)
	{
		// The close codes are documented in class org.java_websocket.framing.CloseFrame
		view.getTextArea().appendText(
				"Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: " + reason +"\n");
	}

	@Override
	public void onError(Exception ex)
	{
		ex.printStackTrace();
		// if the error is fatal then onClose will be called additionally
	}

}