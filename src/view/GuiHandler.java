package view;

import java.net.URI;
import java.net.URISyntaxException;
import controller.WebSocketCl;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 *
 * @author leo
 */
public class GuiHandler
{
	// instance variabele
	private Label label;
	private Label lblIpAddress;
	private TextField txtIpAddress;
	private TextArea textArea;
	private Button btnStart;
	private Button btnStop;
	private WebSocketCl wsclient;
	
	// constructor
	public GuiHandler(GridPane pane)
	{
		// default graphics settings  
        pane.setVgap(5);
        pane.setHgap(8);
        pane.setPadding(new Insets(10,15,20,15));
		
        lblIpAddress = new Label("Websocket address:");
		txtIpAddress = new TextField();
		txtIpAddress.setText("ws://192.168.56.4:8080");

		label = new Label("WebSocket data");
		
		textArea = new TextArea();
		textArea.setMinHeight(200);
		textArea.setMinWidth(500);

		btnStart = new Button("Start");
		btnStop = new Button("Stop");
		
		btnStart.setOnAction(event -> start());
		btnStop.setOnAction(event -> stop());
		
		pane.add(lblIpAddress, 0, 0);
		pane.add(txtIpAddress, 1, 0);
		pane.add(btnStart, 0, 1, 2, 1);
		pane.add(label, 0, 2, 2, 1);
		pane.add(textArea, 0, 3, 2, 1);
		pane.add(btnStop, 0, 4, 2, 1);
	}

	public TextArea getTextArea()
	{
		return textArea;
	}
	
	private void start()
	{	
		
		try
		{
			wsclient = new WebSocketCl(new URI(txtIpAddress.getText()), this);
			wsclient.connect();
		} 
		catch (URISyntaxException ex)
		{
			ex.printStackTrace();
		} 
	}	
	
	private void stop()
	{
		wsclient.close();
	}
	
}
